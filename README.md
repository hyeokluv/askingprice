# 실시간으로 들어오는 매수/매도에 대해서 호가창을 보여주는 웹 애플리케이션

![enter image description here](https://s3.ap-northeast-2.amazonaws.com/externalstore/kakaopay-askingprice-image-2.png)

### 개발 이슈

**실시간 웹을 구현하기 위해 socket.io 를 떠올렸다.**

 - 실시간 기반 앱은 처음 개발해보았지만, socket.io 라이브러리 사용으로 매우 쉽게 개발하였다.

**일반적인 호가창을 위해 어려운 버전을 선택했다.**

 - 쉬운 버전은 정말 쉽게 느껴졌다.
 - 업비트 거래소를 참고했다.
 - 어려운 버전은 막상 개발해보니 쉽진 않았지만, 로직을 손으로 호가창을 수십번 그리면서 개발했다.

**1초에 1건씩 순서대로 입력이 들어온다는 것이 이해하기 힘들었다.**

 - 프론트엔드에서 setInterval 을 통해 1초에 한 번씩 호출하게 했다.
 - 한 번 호출할 때 데이터 1건을 읽기 위한 방법은 flag 를 만들어서 1건씩 처리하기로 했다.


### 개발환경

 - macOS High Sierra v10.13.3
 - npm v5.8.0
 - node.js v9.3.0

### 설치하기

*이 프로젝트를 시작하려면 npm, node.js, git 을 설치하세요.*

그리고 다음 명령어를 통해 git 에서 프로젝트를 복사하세요.
```
git clone https://hyeokluv@bitbucket.org/hyeokluv/askingprice.git
```
복사된 프로젝트로 이동하세요.
```
cd askingprice
```
애플리케이션에 필요한 모듈를 설치합니다.
```
npm install
```
앱을 실행합니다.
```
node bin/www
```
콘솔에서 다음 메시지가 보이면 성공적으로 앱을 실행 했습니다.
```
server start......
```
**http://localhost:3000 으로 접속합니다.**

## 테스트 실행하기

![enter image description here](https://s3.ap-northeast-2.amazonaws.com/externalstore/kakaopay-askingprice-image-1.png)

>  1. "주문내역" 탭에서 자동/수동모드 전환으로 모드를 선택합니다.
>  2. 자동모드: 주문내역 정보를 1초에 1건씩 체결 합니다.
>  3. 수동모드: "체결 호출" 버튼을 통해 1건씩 체결 합니다.
>  4. 호가창에서 체결되는 주문들을 확인 합니다.
>  5. "체결 리셋" 버튼을 통해 주문정보를 처음상태로 초기화 할 수 있습니다.

## 사용 프레임워크 및 라이브러리

* [vue.js](https://github.com/vuejs/vue) - javascript 프레임워크
* [express](https://github.com/expressjs/express) - node.js 웹 애플리케이션 프레임워크
* [bootstrap 4](https://getbootstrap.com/) - UI 라이브러리
* [socket.io](https://socket.io/) - 실시간 이벤트 처리 라이브러리
* [pg-promise](https://github.com/vitaly-t/pg-promise) - PostgreSQL 인터페이스 라이브러리



---