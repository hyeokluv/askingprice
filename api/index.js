const Router = require('express').Router
const router = Router()

const trade = require('./trade')

router.use('/trade', trade)

module.exports = router
