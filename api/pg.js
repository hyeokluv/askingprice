const initOptions = {
  connect: (client, dc, isFresh) => {
    const cp = client.connectionParameters
    console.log('Connect', cp.database)
  },
  disconnect: (client, dc) => {
    const cp = client.connectionParameters
    console.log('Disconnect', cp.database)
  },
  error: (err, e) => {
    if (e.cn) {
      console.log(err)
    }

    if (e.query) {
      // 쿼리 스트링을 사용할 수 있습니다.
      console.log('PG Error Query: ', e.query)
    }

    if (e.params) {
      // 쿼리 파라미터를 사용할 수 있습니다.
      console.log('PG Error Params: ', e.query)
    }

    if (e.ctx) {
      // transaction 또는 task 에서 문제가 발생했습니다.
      console.log('PG Error transaction or Task: ', e.ctx)
    }
  },
  query: e => {
    console.log('Query: ', e.query)
  },
  receive: (data, result, e) => {
    console.log('Data: ', data)
    // console.log('Original Data: ', result)
  },
  task: e => {
    if (e.ctx.finish) {
      // This is a task -> finish event
      console.log('task Duration: ', e.ctx.duration)
      if (e.ctx.success) {
        console.log('task success')
      } else {
        console.log('task faild')
      }
    } else {
      console.log('task Start Time: ', e.ctx.start)
    }
  },
  transact: e => {
    if (e.ctx.finish) {
      // This is a task -> finish event
      console.log('transact Duration: ', e.ctx.duration)
      if (e.ctx.success) {
        console.log('transact success')
      } else {
        console.log('transact faild')
      }
    } else {
      console.log('transact Start Time: ', e.ctx.start)
    }
  },
}

const pgp = require('pg-promise')(initOptions)
const cn = require('../config/database')
const db = pgp(cn)

module.exports = {
  db: db,
  pgp: pgp,
}
