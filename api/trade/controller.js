const db = require('../pg').db

module.exports = {
  initAsk: async (req, res, next) => {
    const { offset } = req.query
    db.task(async function (t) {
      // 호가를 갱신합니다.
      const selectAskBuy = await t.any(`SELECT type,
                                              price,
                                              amount
                                        FROM ask
                                        WHERE type = 'B'
                                        ORDER BY price DESC`)

      const selectAskSell = await t.any(`SELECT type,
                                                  price,
                                                  amount
                                          FROM ask
                                          WHERE type = 'S'
                                          ORDER BY price ASC`)

      return { selectAskBuy, selectAskSell }
    })
      .then(results => {
        res.status(200).json({
          askBuy: results.selectAskBuy,
          askSell: results.selectAskSell,
        })
      })
      .catch(exception => {
        next(exception)
      })
  },
  tradingAsk: async (req, res, next) => {
    const { buyMin, sellMin } = req.query
    console.log('req.query:', req.query)
    db.task(async function (t) {

      // 체결되지 않은 주문 정보를 확인합니다.
      const selectOrderTradeFalse = await t.any(`SELECT seq,
                                                        type,
                                                        price,
                                                        amount
                                                  FROM order_stack
                                                  WHERE trade = false
                                                  ORDER BY seq asc`)

      if (selectOrderTradeFalse.length === 0) {
        // 체결할 주문이 없으면 종료합니다.
        return true
      } else {
        // 체결할 주문이 있습니다.
        const orderSeq = selectOrderTradeFalse[0].seq
        const orderType = selectOrderTradeFalse[0].type
        const orderPrice = selectOrderTradeFalse[0].price
        const orderAmount = selectOrderTradeFalse[0].amount

        if (orderType === 'B') {
          // 매수 주문입니다.
          if (orderPrice <= buyMin) {
            // 매수 주문이 매수 매물 범위 입니다.
            const selectOrder = await t.any(`SELECT *
                                             FROM ask
                                             WHERE price = $1
                                             AND type = 'B'`, [orderPrice])
            if (selectOrder.length > 0) {
              // 매수 주문 가격이 매물에 있습니다. 매물 수량을 더합니다.
              await t.none(`UPDATE ask
                            SET amount = amount + $1
                            WHERE price = $2
                            AND type = 'B'`, [orderAmount, orderPrice])
            } else {
              // 매수 주문 가격이 매물에 없습니다. 매물을 새로 추가합니다.
              await t.none(`INSERT INTO ASK ( type, price, amount, created_date )
                            VALUES ('B', $1, $2, CURRENT_TIMESTAMP)`, [orderPrice, orderAmount])
            }
          } else {
            // 매수 주문이 매도 매물 범위 입니다. (체결 처리를 진행합니다.)
            // 체결을 위한 매물 정보들를 가져옵니다.
            const askTarget = await t.any(`SELECT type,
                                                  price,
                                                  amount
                                            FROM ask
                                            WHERE price >= $1
                                            AND type = 'S'
                                            ORDER BY price ASC`, [sellMin])

            let restOrderAmount = orderAmount

            // 반복하며 매물을 체결합니다.
            for (let i=0; i<askTarget.length; i++) {

              if (askTarget[i].price <= orderPrice) {
                // (매물 가격 <= 주문 가격) 일 때 매물을 삭제 하거나, 업데이트 합니다.

                if (restOrderAmount === askTarget[i].amount) {
                  // 주문 수량과 매물 수량이 같으면 매물을 삭제 후 반복을 종료합니다.
                  await t.none(`DELETE FROM ask
                                WHERE price = $1
                                AND type = 'S'`, [askTarget[i].price])
                  break
                } else if (restOrderAmount - askTarget[i].amount < 0) {
                  // (주문 수량 - 매물 수량) 이 음수이면 체결 처리 후 반복을 종료합니다.
                  await t.none(`UPDATE ask
                                SET amount = amount - $1
                                WHERE price = $2
                                AND type = 'S'`, [restOrderAmount, askTarget[i].price])
                  res.io.emit('sign', {
                    price: askTarget[i].price,
                  })
                  break
                } else if (restOrderAmount - askTarget[i].amount > 0) {
                  // (주문 수량 - 매물 수량) 이 양수이면 체결 처리 후 반복을 계속 진행합니다.
                  await t.none(`DELETE FROM ask
                                WHERE price = $1
                                AND type = 'S'`, [askTarget[i].price])
                  restOrderAmount = restOrderAmount - askTarget[i].amount

                  if (i === (askTarget.length - 1)) {
                    await t.none(`INSERT INTO ASK ( type, price, amount, created_date )
                                  VALUES ('B', $1, $2, CURRENT_TIMESTAMP)`, [orderPrice, restOrderAmount])
                  }

                } 
              } else {
                // (매물 가격 <= 주문 가격) 이 아니면 나머지 주문 매물을 삽입합니다.
                await t.none(`INSERT INTO ASK ( type, price, amount, created_date )
                              VALUES ('B', $1, $2, CURRENT_TIMESTAMP)`, [orderPrice, restOrderAmount])
                break
              }
            }
          }
        } else if (orderType === 'S') {
          // 매도 주문입니다.
          if (orderPrice >= sellMin) {
            // 매도 주문이 매도 매물 범위 입니다.
            const selectOrder = await t.any(`SELECT *
                                             FROM ask
                                             WHERE price = $1
                                             AND type = 'S'`, [orderPrice])
            console.log('selectOrder:', selectOrder)
            if (selectOrder.length > 0) {
              // 매수 주문 가격이 매물에 있습니다. 매물 수량을 더합니다.
              await t.none(`UPDATE ask
                            SET amount = amount + $1
                            WHERE price = $2
                            AND type = 'S'`, [orderAmount, orderPrice])
            } else {
              // 매수 주문 가격이 매물에 없습니다. 매물을 새로 추가합니다.
              await t.none(`INSERT INTO ASK ( type, price, amount, created_date )
                            VALUES ('S', $1, $2, CURRENT_TIMESTAMP)`, [orderPrice, orderAmount])
            }
          } else {
            // 매도 주문이 매수 매물 범위 입니다. (체결 처리를 진행합니다.)
            // 체결을 위한 매물 정보들을 가져옵니다.
            const askTarget = await t.any(`SELECT type,
                                                  price,
                                                  amount
                                            FROM ask
                                            WHERE price <= $1
                                            AND type = 'B'
                                            ORDER BY price DESC`, [buyMin])

            let restOrderAmount = orderAmount

            // 반복하며 매물을 체결합니다.
            for (let i=0; i<askTarget.length; i++) {

              if (askTarget[i].price >= orderPrice) {
                // (매물 가격 >= 주문 가격) 일 때 매물을 삭제 하거나, 업데이트 합니다.

                if (restOrderAmount === askTarget[i].amount) {
                  // 주문 수량과 매물 수량이 같으면 매물을 삭제 후 반복을 종료합니다.
                  await t.none(`DELETE FROM ask
                                WHERE price = $1
                                AND type = 'B'`, [askTarget[i].price])
                  break
                } else if (restOrderAmount - askTarget[i].amount < 0) {
                  // (주문 수량 - 매물 수량) 이 음수이면 체결 처리 후 반복을 종료합니다.
                  await t.none(`UPDATE ask
                                SET amount = amount - $1
                                WHERE price = $2
                                AND type = 'B'`, [restOrderAmount, askTarget[i].price])
                  res.io.emit('sign', {
                    price: askTarget[i].price,
                  })
                  break
                } else if (restOrderAmount - askTarget[i].amount > 0) {
                  // (주문 수량 - 매물 수량) 이 양수이면 체결 처리 후 반복을 계속 진행합니다.
                  await t.none(`DELETE FROM ask
                                WHERE price = $1
                                AND type = 'B'`, [askTarget[i].price])
                  restOrderAmount = restOrderAmount - askTarget[i].amount

                  if (i === (askTarget.length - 1)) {
                    await t.none(`INSERT INTO ASK ( type, price, amount, created_date )
                                  VALUES ('S', $1, $2, CURRENT_TIMESTAMP)`, [orderPrice, restOrderAmount])
                  }
                } 
              } else {
                console.log('(매물 가격 <= 주문 가격) 이 아니면 나머지 주문 매물을 삽입합니다.')
                // (매물 가격 <= 주문 가격) 이 아니면 나머지 주문 매물을 삽입합니다.
                await t.none(`INSERT INTO ASK ( type, price, amount, created_date )
                              VALUES ('S', $1, $2, CURRENT_TIMESTAMP)`, [orderPrice, restOrderAmount])
                break
              }
            }
          }
        }

        // 주문 정보를 체결 처리 합니다.
        await t.none(`UPDATE order_stack
                      SET trade = true
                      WHERE seq = $1`, [orderSeq])
        // 호가를 갱신합니다.
        const selectAskBuy = await t.any(`SELECT type,
                                                  price,
                                                  amount
                                          FROM ask
                                          WHERE type = 'B'
                                          ORDER BY price DESC`)

        const selectAskSell = await t.any(`SELECT type,
                                                    price,
                                                    amount
                                            FROM ask
                                            WHERE type = 'S'
                                            ORDER BY price ASC`)

        const selectOrder = await t.any(`SELECT seq,
                                                  trade,
                                                  type,
                                                  price,
                                                  amount,
                                                  created_date
                                          FROM order_stack
                                          ORDER BY seq ASC`)

        return { selectAskBuy, selectAskSell, selectOrder }

      }
    })
      .then(results => {
        if (results !== true) {
          res.io.emit('trading', {
            askBuy: results.selectAskBuy,
            askSell: results.selectAskSell,
          })
          res.io.emit('get-order', {
            order: results.selectOrder,
          })
        } 
        res.status(200).json({
          success: true,
          message: 'trading',
        })
      })
      .catch(exception => {
        next(exception)
      })
  },
  reset: async (req, res, next) => {
    db.task(async function (t) {
      await t.none(`DELETE FROM order_stack WHERE isdefault IS NULL`)
      await t.none(`UPDATE order_stack
                    SET trade = false`)
      await t.none(`TRUNCATE TABLE ask`)

      // 호가를 갱신합니다.
      const selectAskBuy = await t.any(`SELECT type,
                                              price,
                                              amount
                                        FROM ask
                                        WHERE type = 'B'
                                        ORDER BY price DESC`)

      const selectAskSell = await t.any(`SELECT type,
                                                price,
                                                amount
                                        FROM ask
                                        WHERE type = 'S'
                                        ORDER BY price ASC`)

      const selectOrder = await t.any(`SELECT seq,
                                              trade,
                                              type,
                                              price,
                                              amount,
                                              created_date
                                        FROM order_stack
                                        ORDER BY seq ASC`)
      return { selectAskBuy, selectAskSell, selectOrder }
    })
      .then(results => {
        res.io.emit('trading', {
          askBuy: results.selectAskBuy,
          askSell: results.selectAskSell,
        })
        res.io.emit('get-order', {
          order: results.selectOrder,
        })
        res.status(200).json({
          success: true,
        })
      })
      .catch(exception => {
        next(exception)
      })
  },
  getOrder: async (req, res, next) => {
    db.task(async function (t) {
      const selectOrder = await t.any(`SELECT seq,
                                              trade,
                                              type,
                                              price,
                                              amount,
                                              created_date
                                      FROM order_stack
                                      ORDER BY seq ASC`)
      return { order: selectOrder }
    })
      .then(results => {
        res.status(200).json({
          success: true,
          order: results.order
        })
      })
      .catch(exception => {
        next(exception)
      })
  },
  bundle: async (req, res, next) => {
    const { bundle } = req.body
    console.log('1')
  },
  buy: async (req, res, next) => {
    const { amount, price } = req.body
    db.task(async function (t) {
      await t.none(`INSERT INTO order_stack ( trade, type, price, amount, created_date )
                    VALUES (false, $1, $2, $3, CURRENT_TIMESTAMP)`, [
                      'B',
                      price,
                      amount,
                    ])
      // 호가를 갱신합니다.
      const selectAskBuy = await t.any(`SELECT type,
                                              price,
                                              amount
                                        FROM ask
                                        WHERE type = 'B'
                                        ORDER BY price DESC`)

      const selectAskSell = await t.any(`SELECT type,
                                                price,
                                                amount
                                        FROM ask
                                        WHERE type = 'S'
                                        ORDER BY price ASC`)

      const selectOrder = await t.any(`SELECT seq,
                                              trade,
                                              type,
                                              price,
                                              amount,
                                              created_date
                                        FROM order_stack
                                        ORDER BY seq ASC`)
      return { selectAskBuy, selectAskSell, selectOrder }
    })
      .then(results => {
        res.io.emit('trading', {
          askBuy: results.selectAskBuy,
          askSell: results.selectAskSell,
        })
        res.io.emit('get-order', {
          order: results.selectOrder,
        })
        res.status(200).json({
          success: true,
        })
      })
      .catch(exception => {
        next(exception)
      })
  },
  sell: async (req, res, next) => {
    const { amount, price } = req.body
    db.task(async function (t) {
      await t.none(`INSERT INTO ask ( trade, type, price, amount, created_date )
                    VALUES (false, $1, $2, $3, CURRENT_TIMESTAMP)`, [
                      'S',
                      price,
                      amount,
                    ])
      // 호가를 갱신합니다.
      const selectAskBuy = await t.any(`SELECT type,
                                              price,
                                              amount
                                        FROM ask
                                        WHERE type = 'B'
                                        ORDER BY price DESC`)

      const selectAskSell = await t.any(`SELECT type,
                                                price,
                                                amount
                                        FROM ask
                                        WHERE type = 'S'
                                        ORDER BY price ASC`)

      const selectOrder = await t.any(`SELECT seq,
                                              trade,
                                              type,
                                              price,
                                              amount,
                                              created_date
                                        FROM order_stack
                                        ORDER BY seq ASC`)
      return { selectAskBuy, selectAskSell, selectOrder }
    })
      .then(results => {
        res.io.emit('trading', {
          askBuy: results.selectAskBuy,
          askSell: results.selectAskSell,
        })
        res.io.emit('get-order', {
          order: results.selectOrder,
        })
        res.status(200).json({
          success: true,
        })
      })
      .catch(exception => {
        next(exception)
      })
  },
}
