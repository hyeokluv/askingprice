const Router = require('express')
const router = new Router()

const controller = require('./controller')

router.get('/ask/init', controller.initAsk)
router.get('/ask/trading', controller.tradingAsk)

router.post('/reset', controller.reset)
router.get('/order/get', controller.getOrder)

router.post('/bundle', controller.bundle)
router.post('/buy', controller.buy)
router.post('/sell', controller.sell)

module.exports = router
