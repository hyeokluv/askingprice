var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var app = express()

var server = require('http').Server(app)
var io = require('socket.io')(server)

require('dotenv').config()

app.use(require('connect-history-api-fallback')())

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(function(req, res, next){
  res.io = io
  next()
})

// Import API Routes
const api = require('./api')
app.use('/api', api)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

io.on('connection', function (socket) {
  // socket.emit('request', /* */) // emit an event to the socket
  // io.emit('broadcast', /* */) // emit an event to all connected sockets
  socket.on('pingServer', function (data) {
    console.log('>>>', data)
  }) // listen to the event
})

module.exports = { app: app, server: server }
